package com.example.com.discountapp;

import android.content.Context;

import dmax.dialog.SpotsDialog;

public class ProgressDialogClass {

    public static  SpotsDialog  progressDialog ;

    public static void showProgress(Context context)
    {
        progressDialog = new SpotsDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideProgress()
    {
        progressDialog.dismiss();
    }
}