package com.example.com.discountapp.Adapter;



import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.com.discountapp.Model.ViewAd;
import com.example.com.discountapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterViewAd extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    ArrayList<ViewAd> data;

    public AdapterViewAd(Activity mainActivity, ArrayList<ViewAd> data) {
        // TODO Auto-generated constructor stub
        this.data = data;
        context = mainActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_viewaddlist, null);
        holder.code =  rowView.findViewById(R.id.tv_code);
        holder.phone_number = rowView.findViewById(R.id.tv_phoneno);
        holder.product_name = rowView.findViewById(R.id.tv_pname);
        holder.description = rowView.findViewById(R.id.tv_description);
        holder.price_before = rowView.findViewById(R.id.tv_pricebefore);
        holder.price_after = rowView.findViewById(R.id.tv_priceafter);
        holder.discount = rowView.findViewById(R.id.tv_discount);
        holder.store_name = rowView.findViewById(R.id.tv_storename);
        holder.address = rowView.findViewById(R.id.tv_address);
        holder.expiry_date = rowView.findViewById(R.id.tv_expirydate);
        holder.country = rowView.findViewById(R.id.tv_country);
        holder.city = rowView.findViewById(R.id.tv_city);
        holder.img1 = rowView.findViewById(R.id.img_view_1);
        holder.img2 = rowView.findViewById(R.id.img_view_2);
        holder.img3 = rowView.findViewById(R.id.img_view_3);
        holder.img4 = rowView.findViewById(R.id.img_view_4);

        ViewAd current = data.get(position);
        holder.code.setText("code: "+current.getCode());
        holder.phone_number.setText("phone no: "+current.getPhone_number());
        holder.product_name.setText("product name: "+current.getProduct_name());
        holder.description.setText("description: "+current.getDescription());
        holder.price_before.setText("price before: "+current.getPrice_before());
        holder.price_after.setText("price after: "+current.getPrice_after());
        holder.discount.setText("discount: "+current.getDiscount());
        holder.store_name.setText("store name: "+current.getStore_name());
        holder.address.setText("address: "+current.getAddress());
        holder.expiry_date.setText("expiry date: "+current.getExpiry_date());
        holder.country.setText("country: "+current.getCountry());
        holder.city.setText("city: "+current.getCity());

       /* if (!current.getImage_1_url().equals("")){
            Picasso.with(context)
                    .load(current.getImage_1_url())
                    .fit()
                    .into(holder.img1);
                 }

        if (!current.getImage_2_url().equals("")){
            Picasso.with(context)
                    .load(current.getImage_2_url())
                    .fit()
                    .into(holder.img2);
        }*/

        if (!current.getImage_3_url().equals("")){
            Picasso.with(context)
                    .load(current.getImage_3_url())
                    .fit()
                    .into(holder.img3);
        }

        if (!current.getImage_4_url().equals("")){
            Picasso.with(context)
                    .load(current.getImage_4_url())
                    .fit()
                    .into(holder.img4);
        }

        return rowView;
    }

    public class Holder {
        TextView code,phone_number,product_name,description,price_before,price_after,discount,store_name,address,expiry_date,country,city;
        ImageView img1,img2,img3,img4;
    }

}
