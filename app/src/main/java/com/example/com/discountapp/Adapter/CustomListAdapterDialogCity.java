package com.example.com.discountapp.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.com.discountapp.R;

import java.util.ArrayList;

public class CustomListAdapterDialogCity extends BaseAdapter implements Filterable {

    private ArrayList<String> listData;

    private ArrayList<String> listDataCopy;

    private LayoutInflater layoutInflater;

    public CustomListAdapterDialogCity(Context context, ArrayList<String> listData) {
        this.listData = listData;
        this.listDataCopy = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_dialog, null);
            holder = new ViewHolder();
            holder.unitView = (TextView) convertView.findViewById(R.id.unit);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.unitView.setText(listData.get(position).toString());

        return convertView;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter(){

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<String> FilteredArrayNames = new ArrayList<String>();

                // perform your search here using the searchConstraint String.

                Log.d("constraintconstraint","constraint: "+constraint);

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < listData.size(); i++) {
                    String dataNames = listData.get(i);
                    if (dataNames.toLowerCase().startsWith(constraint.toString()))  {
                        FilteredArrayNames.add(dataNames);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;
                Log.e("VALUES", results.values.toString());

                return results;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                listData = (ArrayList<String>) filterResults.values;
                if(charSequence.equals("")){
                    listData = listDataCopy;
                }
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    static class ViewHolder {
        TextView unitView;
    }

}