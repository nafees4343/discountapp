package com.example.com.discountapp;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.com.discountapp.Adapter.AdapterViewAd;
import com.example.com.discountapp.Model.ViewAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewActivity extends AppCompatActivity {

    ArrayList<ViewAd> list;
    ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        Toolbar toolbar = findViewById(R.id.toolbar_view);
        toolbar.setTitle("DiscountApp");
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_left_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();

            }
        });

        lv = (ListView) findViewById(R.id.lisview);
        get_profile_data();
    }

    public void get_profile_data() {
        ProgressDialogClass.showProgress(ViewActivity.this);
        AndroidNetworking.get("http://goodwisesearch.com/Discountandroidapp/getdiscounts.php")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONArray response) {
                        ProgressDialogClass.hideProgress();
                        Log.d("ViewAdresponse","res:"+response);
                        boolean status = false;
                        String img_tag = "http://goodwisesearch.com/uploads/";

                        try {
                            JSONArray jsonArray = new JSONArray(response.toString());
                            Log.d("asddaad",""+jsonArray.length());
                            list = new ArrayList<>();
                            for(int i=0; i<jsonArray.length(); i++) {
                                    JSONObject data = jsonArray.getJSONObject(i);
                                    String code = (String) data.get("code");
                                    //Toast.makeText(ViewActivity.this, "code:" + code, Toast.LENGTH_LONG).show();

                                    String phone_number = (String) data.get("phone_number");
                                    String product_name = (String) data.get("product_name");
                                    String description = (String) data.get("description");
                                    String price_before = (String) data.get("price_before");
                                    String price_after = (String) data.get("price_after");
                                    String discount = (String) data.get("discount");
                                    String store_name = (String) data.get("store_name");
                                    String address = (String) data.get("address");
                                    String expiry_date = (String) data.get("expiry_date");
                                    String country = (String) data.get("country");
                                    String city = (String) data.get("city");
                                    String image_1_url = (String) img_tag + data.get("image_1_url");
                                    String image_2_url = (String) img_tag + data.get("image_2_url");
                                    String image_3_url = (String) img_tag + data.get("image_3_url");
                                    String image_4_url = (String) img_tag + data.get("image_4_url");

                                    ViewAd viewAd = new ViewAd(code, phone_number, product_name, description, price_before, price_after, discount, store_name, address, expiry_date, country, city, image_1_url, image_2_url, image_3_url, image_4_url);
                                    list.add(viewAd);
                                    //Toast.makeText(ViewActivity.this, "code:" + code, Toast.LENGTH_LONG).show();
                            }

                            AdapterViewAd adapter = new AdapterViewAd(ViewActivity.this, list);
                            lv.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ViewActivity.this, "code:" , Toast.LENGTH_LONG).show();

                        }



                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.hideProgress();
                        Toast.makeText(ViewActivity.this, "" + anError, Toast.LENGTH_LONG).show();

                    }
                });
    }
}
