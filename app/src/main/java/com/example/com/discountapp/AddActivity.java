package com.example.com.discountapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.com.discountapp.Adapter.CustomListAdapterDialog;
import com.example.com.discountapp.Adapter.CustomListAdapterDialogCity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {

    Bitmap bitmap;
    ByteArrayOutputStream bStream;
    byte[] byteArray;
    ImageView img,img1,img2,img3,img4;
    EditText et_phoneno,et_code,et_productname,et_description,et_pricebefore,et_priceafter,et_discount,et_storename,et_address,et_expirydate,et_country,et_city;
    Button btn_add_submit,btn_country,btn_city,btn_ok;
    byte[] byteArray1,byteArray2,byteArray3,byteArray4;
    Bitmap bitmap1,bitmap2,bitmap3,bitmap4;
    int i;
    List<String> listOfCountry;
    List<String> listOfCity;
    Dialog dialog;
    ListView lvcounties,lvcity;
    EditText et_searchCountry,et_searchCity;
    CustomListAdapterDialog clad;
    CustomListAdapterDialogCity cladd;
    JSONArray itemArray;
    JSONObject obj;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Toolbar toolbar = findViewById(R.id.toolbar_add);
        toolbar.setTitle("DiscountApp");
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_left_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();

            }
        });


        et_phoneno = findViewById(R.id.et_phoneno);
        et_code = findViewById(R.id.et_code);
        et_productname = findViewById(R.id.et_productname);
        et_description = findViewById(R.id.et_description);
        et_priceafter = findViewById(R.id.et_priceafter);
        et_pricebefore = findViewById(R.id.et_pricebefore);
        et_discount = findViewById(R.id.et_discount);
        et_storename = findViewById(R.id.et_storename);
        et_address = findViewById(R.id.et_address);
        et_expirydate = findViewById(R.id.et_expirydate);
        btn_country = findViewById(R.id.btn_country);
        btn_city = findViewById(R.id.btn_city);
        btn_ok = findViewById(R.id.btn_ok);

        btn_country.setOnClickListener(this);
        btn_city.setOnClickListener(this);
        btn_ok.setOnClickListener(this);

        img1 = findViewById(R.id.img_add_1);
        img2 = findViewById(R.id.img_add_2);
        img3 = findViewById(R.id.img_add_3);
        img4 = findViewById(R.id.img_add_4);

        btn_add_submit = findViewById(R.id.btn_add_submit);

        myCalendar = Calendar.getInstance();

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);

        et_expirydate.setOnClickListener(this);
        btn_add_submit.setOnClickListener(this);



        try {

            listOfCountry = new ArrayList<>();
            Log.d("Detailsssss", "list:::"+loadJSONFromAsset());
            obj = new JSONObject(loadJSONFromAsset());
            Log.d("lnlnlnlnln","length:"+obj.length());
            Iterator<String> keys = obj.keys();
            while (keys.hasNext()){

                String country = keys.next();
                Log.d("coountry","Country: "+country);
                listOfCountry.add(country);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


    }



    private void showDialogCountries(){

        dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_main, null);

        lvcounties = (ListView) view.findViewById(R.id.custom_list);

        et_searchCountry = view.findViewById(R.id.et_searchCountry);

        // Change MyActivity.this and myListOfItems to your own values
        clad = new CustomListAdapterDialog(AddActivity.this, (ArrayList<String>) listOfCountry);

        lvcounties.setAdapter(clad);

        dialog.setCancelable(true);

        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

    }


    private void showDialogCity(){

        dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_main_city, null);

        lvcity = (ListView) view.findViewById(R.id.custom_list_city);

        et_searchCity = view.findViewById(R.id.et_searchCity);

        // Change MyActivity.this and myListOfItems to your own values
        cladd = new CustomListAdapterDialogCity(AddActivity.this, (ArrayList<String>) listOfCity);

        lvcity.setAdapter(cladd);

        dialog.setCancelable(true);

        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        dialog.show();


    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("countriesToCities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }



    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                AddActivity.this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1);

                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, 0);
                    }
                });
        myAlertDialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {

                    bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    bStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                    byteArray = bStream.toByteArray();
                    switch (i){
                        case 1:
                            byteArray1 = byteArray;
                            bitmap1 = BitmapFactory.decodeByteArray(byteArray1, 0, byteArray1.length);
                            break;

                        case 2:
                            byteArray2 = byteArray;
                            bitmap2 = BitmapFactory.decodeByteArray(byteArray2, 0, byteArray2.length);

                            break;

                        case 3:
                            byteArray3 = byteArray;
                            bitmap3 = BitmapFactory.decodeByteArray(byteArray3, 0, byteArray3.length);

                            break;

                        case 4:
                            byteArray4 = byteArray;
                            bitmap4 = BitmapFactory.decodeByteArray(byteArray4, 0, byteArray4.length);

                            break;
                    }
                    img.setImageBitmap(bitmap);

                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
//                    img_profile_image.setImageURI(selectedImage);

                    bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                    byteArray = bStream.toByteArray();
                    switch (i){
                        case 1:
                            byteArray1 = byteArray;
                            Log.d("byteArray1byteArray1",""+byteArray1);
                            bitmap1 = BitmapFactory.decodeByteArray(byteArray1, 0, byteArray1.length);

                            break;

                        case 2:
                            byteArray2 = byteArray;
                            Log.d("byteArray2byteArray2",""+byteArray2);
                            bitmap2 = BitmapFactory.decodeByteArray(byteArray2, 0, byteArray2.length);

                            break;

                        case 3:
                            byteArray3 = byteArray;
                            Log.d("byteArray3byteArray3",""+byteArray3);
                            bitmap3 = BitmapFactory.decodeByteArray(byteArray3, 0, byteArray3.length);

                            break;

                        case 4:
                            byteArray4 = byteArray;
                            bitmap4 = BitmapFactory.decodeByteArray(byteArray4, 0, byteArray4.length);

                            break;
                    }
                    img.setImageBitmap(bitmap);

                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_add_1:
                i = 1;
                img = img1;
                byteArray = byteArray1;
                startDialog();


                break;

            case R.id.img_add_2:
                i = 2;
                img = img2;
                byteArray = byteArray2;
                startDialog();


                break;
            case R.id.img_add_3:
                i = 3;
                img = img3;
                byteArray = byteArray3;
                startDialog();


                break;
            case R.id.img_add_4:
                i = 4;
                img = img4;
                byteArray = byteArray4;
                startDialog();


                break;
            case R.id.btn_add_submit:

                    senddata();

                break;

            case R.id.btn_country:
                /*spinner_country.performClick();
                spinner_country.setOnItemSelectedListener(this);*/
                btn_city.setText("");

                showDialogCountries();

                lvcounties.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Log.d("listOfCountry","Selected: "+clad.getItem(i));
                        btn_country.setText(""+clad.getItem(i));
                        dialog.cancel();
                    }
                });

                et_searchCountry.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        Log.d("searchCC",""+charSequence);
                        AddActivity.this.clad.getFilter().filter(charSequence);

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                Log.d("btnbtn","btn:"+btn_country.getText().toString());
                break;

            case R.id.btn_city:

                if(!btn_country.getText().toString().equals("")) {

                    listOfCity = new ArrayList<>();

                    try {
                        itemArray = obj.getJSONArray(btn_country.getText().toString());
                        Log.d("size:size:size:", "size: " + itemArray.length());
                        listOfCountry.removeAll(Arrays.asList(null,""));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < itemArray.length(); i++) {
                        try {
                            Log.i("Valueis:::", "" + itemArray.get(i));
                            listOfCity.add((String) itemArray.get(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    listOfCity.removeAll(Arrays.asList(null,""));

                    if(listOfCity.size()==0){
                        Toast.makeText(AddActivity.this, "no cities found", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    showDialogCity();

                    lvcity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("listOfCountry","Selected: "+cladd.getItem(i));
                            btn_city.setText(""+cladd.getItem(i));
                            dialog.cancel();
                        }
                    });


                    et_searchCity.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            Log.d("searchCC",""+charSequence);
                            AddActivity.this.cladd.getFilter().filter(charSequence);

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                }
                else {
                    Toast.makeText(AddActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.et_expirydate:
                new DatePickerDialog(AddActivity.this,R.style.DialogTheme, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btn_ok:
                finish();
                break;
        }
    }
    

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        et_expirydate.setText(sdf.format(myCalendar.getTime()));
    }

    public File returnFile(Bitmap bmp) throws IOException {
        File f = new File(getCacheDir(), ".png");
        f.createNewFile();
        //Convert bitmap to byte array
        Bitmap bitmap = bmp;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    public void senddata(){



        String phoneno,code,productname,description,pricebefore,priceafter,discount,storename,address,expirydate,country,city;

        phoneno = et_phoneno.getText().toString();
        code = et_code.getText().toString();
        productname = et_productname.getText().toString();
        description = et_description.getText().toString();
        pricebefore = et_pricebefore.getText().toString();
        priceafter = et_priceafter.getText().toString();
        discount = et_discount.getText().toString();
        storename = et_storename.getText().toString();
        address = et_address.getText().toString();
        expirydate = et_expirydate.getText().toString();
        country = btn_country.getText().toString();
        city = btn_city.getText().toString();

        if(phoneno.equals("")){
            Toast.makeText(AddActivity.this, "phone no is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(code.equals("")){
            Toast.makeText(AddActivity.this, "code is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(productname.equals("")){
            Toast.makeText(AddActivity.this, "product name is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(description.equals("")){
            Toast.makeText(AddActivity.this, "description is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(pricebefore.equals("")){
            Toast.makeText(AddActivity.this, "price before is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(priceafter.equals("")){
            Toast.makeText(AddActivity.this, "price after is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(discount.equals("")){
            Toast.makeText(AddActivity.this, "discount is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(storename.equals("")){
            Toast.makeText(AddActivity.this, "store name is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(address.equals("")){
            Toast.makeText(AddActivity.this, "address is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(expirydate.equals("")){
            Toast.makeText(AddActivity.this, "expiry date is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(country.equals("")){
            Toast.makeText(AddActivity.this, "country is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(city.equals("")){
            Toast.makeText(AddActivity.this, "city no is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(byteArray1==null){
            Toast.makeText(AddActivity.this, "Image no 1 is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(byteArray2==null){
            Toast.makeText(AddActivity.this, "Image no 2 is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(byteArray3==null){
            Toast.makeText(AddActivity.this, "Image no 3 is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(byteArray4==null){
            Toast.makeText(AddActivity.this, "Image no 4 is missing", Toast.LENGTH_SHORT).show();
            return;
        }



        if (NetworkConnectivityClass.isNetworkAvailable(AddActivity.this)) {

            Log.d("asdasdaads",""+byteArray1);

            if(!phoneno.equals("")&& !code.equals("") && !productname.equals("") && !description.equals("") &&
                    !pricebefore.equals("") && !priceafter.equals("") && !discount.equals("") && !storename.equals("")
                    && !address.equals("") && !expirydate.equals("") && !country.equals("") && !city.equals("")
                    && byteArray1!=null&& byteArray2!=null&& byteArray3!=null&& byteArray4!=null) {


                /*bitmap2 = BitmapFactory.decodeByteArray(byteArray2, 0, byteArray2.length);
                bitmap3 = BitmapFactory.decodeByteArray(byteArray3, 0, byteArray3.length);
                bitmap4 = BitmapFactory.decodeByteArray(byteArray4, 0, byteArray4.length);*/

                ProgressDialogClass.showProgress(AddActivity.this);



                //***********post user list********
                try {
                    AndroidNetworking.upload("http://goodwisesearch.com/Discountandroidapp/savediscount.php")
                            .addMultipartParameter("phoneno", phoneno)
                            .addMultipartParameter("code", code)
                            .addMultipartParameter("productname", productname)
                            .addMultipartParameter("description", description)
                            .addMultipartParameter("pricebefore", pricebefore)
                            .addMultipartParameter("priceafter", priceafter)
                            .addMultipartParameter("discount", discount)
                            .addMultipartParameter("storename", storename)
                            .addMultipartParameter("address", address)
                            .addMultipartParameter("expirydate", expirydate)
                            .addMultipartParameter("country", country)
                            .addMultipartParameter("city", city)
                            .addMultipartFile("image_1", returnFile(bitmap1))
                            .addMultipartFile("image_2", returnFile(bitmap2))
                            .addMultipartFile("image_3", returnFile(bitmap3))
                            .addMultipartFile("image_4", returnFile(bitmap4))

                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("asdasdas","rr;"+response);
                                    ProgressDialogClass.hideProgress();
                                    boolean success = false;
                                    try {
                                        success = response.getBoolean("success");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    if (success) {
                                        Toast.makeText(AddActivity.this, "Submitted", Toast.LENGTH_LONG).show();
                                        btn_add_submit.setVisibility(View.GONE);
                                        btn_ok.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        Toast.makeText(AddActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onError(ANError error) {
                                    Log.v("SignIn", "" + error);
                                    ProgressDialogClass.hideProgress();
                                    //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                                }
                            });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(AddActivity.this,"Some thing is missing",Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(AddActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }




    }
}
