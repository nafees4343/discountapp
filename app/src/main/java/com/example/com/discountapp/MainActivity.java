package com.example.com.discountapp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.com.discountapp.Adapter.CustomListAdapterDialog;
import com.example.com.discountapp.Adapter.CustomListAdapterDialogCity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    android.support.v7.widget.Toolbar toolbar;
    Spinner spinner_country,spinner_city;
    List<String> listOfCountry;
    List<String> listOfCity;
    int count=0;
    Button btn_country,btn_city;
    Intent i;
    ImageView img_add,img_view,img_search;
    ListView lvcounties,lvcity;
    EditText et_searchCountry,et_searchCity;
    Dialog dialog;
    JSONObject obj;
    JSONArray itemArray;
    CustomListAdapterDialog clad;
    CustomListAdapterDialogCity cladd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar_mainactivity);
        setSupportActionBar(toolbar);
        toolbar.setTitle("DiscountApp");



        btn_country = findViewById(R.id.btn_spinner_country);

        btn_city = findViewById(R.id.btn_spinner_city);

        img_add = findViewById(R.id.img_add);
        img_search = findViewById(R.id.img_search);
        img_view = findViewById(R.id.img_view);


        btn_country.setOnClickListener(this);
        btn_city.setOnClickListener(this);
        img_add.setOnClickListener(this);
        img_view.setOnClickListener(this);
        img_search.setOnClickListener(this);



        try {

            listOfCountry = new ArrayList<>();
            Log.d("Detailsssss", "list:::"+loadJSONFromAsset());
            obj = new JSONObject(loadJSONFromAsset());
            Log.d("lnlnlnlnln","length:"+obj.length());
            Iterator<String> keys = obj.keys();
            while (keys.hasNext()){

                String country = keys.next();
                Log.d("coountry","Country: "+country);
                listOfCountry.add(country);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private void showDialogCountries(){

        dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_main, null);

         lvcounties = (ListView) view.findViewById(R.id.custom_list);

         et_searchCountry = view.findViewById(R.id.et_searchCountry);

        listOfCountry.removeAll(Arrays.asList(null,""));


        // Change MyActivity.this and myListOfItems to your own values
        clad = new CustomListAdapterDialog(MainActivity.this, (ArrayList<String>) listOfCountry);

        lvcounties.setAdapter(clad);

        dialog.setCancelable(true);

        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

    }


    private void showDialogCity(){

        dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_main_city, null);

        lvcity = (ListView) view.findViewById(R.id.custom_list_city);

        et_searchCity = view.findViewById(R.id.et_searchCity);

        // Change MyActivity.this and myListOfItems to your own values
         cladd = new CustomListAdapterDialogCity(MainActivity.this, (ArrayList<String>) listOfCity);

        lvcity.setAdapter(cladd);

        dialog.setCancelable(true);

        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        dialog.show();


    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("countriesToCities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_spinner_country:
                /*spinner_country.performClick();
                spinner_country.setOnItemSelectedListener(this);*/
                btn_city.setText("");
                showDialogCountries();

                lvcounties.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Log.d("listOfCountry","Selected: "+clad.getItem(i));
                        btn_country.setText(""+clad.getItem(i));
                        dialog.cancel();
                    }
                });

                et_searchCountry.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        Log.d("searchCC",""+charSequence);
                        MainActivity.this.clad.getFilter().filter(charSequence);

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                Log.d("btnbtn","btn:"+btn_country.getText().toString());
                break;

            case R.id.btn_spinner_city:

                if(!btn_country.getText().toString().equals("")) {

                    listOfCity = new ArrayList<>();

                    try {
                        itemArray = obj.getJSONArray(btn_country.getText().toString());
                        //Log.d("size:size:size:", "size: " + itemArray.length());
                        Log.d("sizesada", "size: " + itemArray.length());
                        listOfCountry.removeAll(Arrays.asList(null,""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                        for (int i = 0; i < itemArray.length(); i++) {
                            try {
                                Log.i("Valueis:::", "" + itemArray.get(i));
                                listOfCity.add((String) itemArray.get(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    listOfCity.removeAll(Arrays.asList(null,""));

                    if(listOfCity.size()==0){
                        Toast.makeText(MainActivity.this, "no cities found", Toast.LENGTH_SHORT).show();
                        return;
                    }

                       showDialogCity();


                    lvcity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("listOfCountry","Selected: "+cladd.getItem(i));
                            btn_city.setText(""+cladd.getItem(i));
                            dialog.cancel();
                        }
                    });


                    et_searchCity.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            Log.d("searchCC",""+charSequence);
                            MainActivity.this.cladd.getFilter().filter(charSequence);

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                }
                else {
                    Toast.makeText(MainActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_add:
                i = new Intent(MainActivity.this,AddActivity.class);
                startActivity(i);
                break;
            case R.id.img_view:
                i = new Intent(MainActivity.this,ViewActivity.class);
                startActivity(i);
                break;
            case R.id.img_search:
                String country = btn_country.getText().toString();
                String city = btn_city.getText().toString();
                if(country.equals("")){
                    Toast.makeText(MainActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(city.equals("")){
                    Toast.makeText(MainActivity.this, "Please select city first", Toast.LENGTH_SHORT).show();
                    return;
                }

                i = new Intent(MainActivity.this, SearchActivity.class);
                i.putExtra("country",country);
                i.putExtra("city",city);
                startActivity(i);

                break;
        }
    }
}
