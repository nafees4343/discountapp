package com.example.com.discountapp.Model;

public class ViewAd {

/* "code": "2342",
         "phone_number": "03",
         "product_name": "efsd",
         "description": "sdfsdf",
         "price_before": "242",
         "price_after": "2342",
         "discount": "23",
         "store_name": "sdfs",
         "address": "sdfsd",
         "expiry_date": "1220",
         "country": "sda",
         "city": "sda",
         "district": null,
         "main_image": null,
         "image_1": "",
         "image_2": "",
         "image_3": "",
         "image_4": "",
         "image_1_url": "1543830709ic_add_docs.png",
         "image_2_url": "1543830709ic_add_docs.png",
         "image_3_url": "1543830709ic_search.png",
         "image_4_url": "1543830709ic_view.PNG"*/

    public String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getProduct_name() {
        return product_name;
    }

    public ViewAd(String code, String phone_number, String product_name, String description, String price_before, String price_after, String discount, String store_name, String address, String expiry_date, String country, String city,String image_1_url,String image_2_url, String image_3_url, String image_4_url) {
        this.code = code;
        this.phone_number = phone_number;
        this.product_name = product_name;
        this.description = description;
        this.price_before = price_before;
        this.price_after = price_after;
        this.discount = discount;
        this.store_name = store_name;
        this.address = address;
        this.expiry_date = expiry_date;
        this.country = country;
        this.city = city;
        this.image_1_url = image_1_url;
        this.image_2_url = image_2_url;
        this.image_3_url = image_3_url;
        this.image_4_url = image_4_url;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice_before() {
        return price_before;
    }

    public void setPrice_before(String price_before) {
        this.price_before = price_before;
    }

    public String getPrice_after() {
        return price_after;
    }

    public void setPrice_after(String price_after) {
        this.price_after = price_after;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public String phone_number;
    public String product_name;
    public String description;
    public String price_before;
    public String price_after;
    public String discount;
    public String store_name;
    public String address;
    public String expiry_date;
    public String country;
    public String city;
    public String image_1_url;
    public String image_2_url;
    public String image_3_url;

    public String getImage_1_url() {
        return image_1_url;
    }

    public void setImage_1_url(String image_1_url) {
        this.image_1_url = image_1_url;
    }

    public String getImage_2_url() {
        return image_2_url;
    }

    public void setImage_2_url(String image_2_url) {
        this.image_2_url = image_2_url;
    }

    public String getImage_3_url() {
        return image_3_url;
    }

    public void setImage_3_url(String image_3_url) {
        this.image_3_url = image_3_url;
    }

    public String getImage_4_url() {
        return image_4_url;
    }

    public void setImage_4_url(String image_4_url) {
        this.image_4_url = image_4_url;
    }

    public String image_4_url;

}
